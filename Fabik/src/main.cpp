#include <stdio.h>
#include <vector>

#include <GL/gl3w.h>            
#include <glfw/glfw3.h>

#include "ui/ui.h"
#include "ui/iview.h"

#include "target2d.h"
#include "fabik_view.h"
#include "fabik_options_view.h"

int initial_window_width = 640;
int initial_window_height = 320;

glm::vec2 window_size((float)initial_window_width, (float)initial_window_height);

static void glfw_error_callback(int error, const char* description) {
	fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

void glfw_window_size_callback(GLFWwindow* window, int width, int height) {
	window_size = glm::vec2((float)width, (float)height);
}

int main() {
	glfwSetErrorCallback(glfw_error_callback);
	if (!glfwInit())
		exit(EXIT_FAILURE);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	GLFWwindow* window = glfwCreateWindow(initial_window_width, initial_window_height, "Ocbj - FABIK Inverse Kinematics", nullptr, nullptr);

	if (window == nullptr)
		exit(EXIT_FAILURE);

	glfwMakeContextCurrent(window);
	glfwSetWindowSizeCallback(window, glfw_window_size_callback);

	glfwSwapInterval(1); 

	if (gl3wInit() != 0) {
		fprintf(stderr, "Failed to initialize gl3w!\n");
		exit(EXIT_FAILURE);
	}

	ocbj::target_2d target;
	
	ocbj::ui ui("#version 330", window);
	std::vector<ocbj::iview*> views;

	ocbj::fabik_view view;
	ocbj::fabik_options_view options_view;

	view.Init(target);
	options_view.Init(&view);

	views.emplace_back(&view);
	views.emplace_back(&options_view);

	glClearColor(0.12F, 0.12F, 0.12F, 1.0F);

	double deltaTime = 0.0F;
	double prevElapsedTime = (double)glfwGetTime();

	while (!glfwWindowShouldClose(window)) {
		glfwPollEvents(); 

		double elapsedTime = glfwGetTime();
		deltaTime = elapsedTime - prevElapsedTime;
		prevElapsedTime = elapsedTime;

		glClear(GL_COLOR_BUFFER_BIT);

		target.Update(deltaTime, elapsedTime, window_size);

		ui.Begin();
			ui.Update(deltaTime, elapsedTime, window_size, views);
			ui.Draw(views);
		ui.End();

		glfwSwapBuffers(window);
	}

	glfwDestroyWindow(window);
	glfwTerminate();

	exit(EXIT_SUCCESS);
}
