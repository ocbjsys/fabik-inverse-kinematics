#pragma once
#include <glm/glm.hpp>

#include "ui/iview.h"

#include "fabik2d.h"

namespace ocbj {
	struct target_2d;

	class fabik_view : public iview {
	public:
		void Init(ocbj::target_2d& target);
		void Draw() const override;
		void Update(double deltaTime, double elapsedTime, const glm::vec2& size) override;

		void AddBones(int count);
		void RemoveBones(int count);
		void SetBoneLength(float length);
		float BoneLength();
		int NumberOfBones();

	private:
		ocbj::target_2d* _target;
		glm::vec3 _target_color = glm::vec3(255.0F, 255.0F, 255.0F);
		glm::vec2 _size;

		float _inital_bone_length = 22.0F;
		int _inital_number_of_bones = 12;
		std::vector<bone_2d> _bones;
	};
}
