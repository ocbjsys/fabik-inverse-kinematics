#pragma once
#include <glm/glm.hpp>

#include "fabik_view.h"

namespace ocbj {
	class fabik_options_view : public iview {
	public:
		void Init(fabik_view* fabik_view_);
		void Draw() const override;
		void Update(double deltaTime, double elapsedTime, const glm::vec2& size) override;

	private:
		fabik_view* _fabik_view;
	};
}
