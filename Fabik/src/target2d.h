#pragma once
#include <glm/glm.hpp>

namespace ocbj {
	struct target_2d {
		float speed;
		glm::vec2 position;
		glm::vec2 direction;

		target_2d(float speed_ = 200.0F,
			const glm::vec2& position_ = glm::vec2(0.0F, 0.0F),
			const glm::vec2& direction_ = glm::vec2(1.0F, 1.0F));

		void Update(double deltaTime, double elapsedTime, const glm::vec2& bounds);
	};
}
