#include <ranges>

#include "fabik2d.h"

constexpr float kPI = 3.14159265358979323846F;

// Converts an angle to radians
constexpr float kDeg2Rad(float angleInDegrees) {
	return angleInDegrees * (kPI / 180.0F);
}
// Converts an angle to degrees
constexpr float kRad2Deg(float angleInRadians) {
	return angleInRadians * (180.0F / kPI);
}

static glm::vec2 polarToCartesian(float angle, float length) {
	return glm::vec2(length * cos(angle), length * sin(angle));
}

ocbj::bone_2d::bone_2d(const bone_2d& parent, float length_, float angle_)
	: a(parent.b),
	length(length_),
	angle(angle_)
{
	b = CalcB(a, length_, angle_);
}

ocbj::bone_2d::bone_2d(const glm::vec2& a_, float length_, float angle_)
	: a(a_),
	length(length_),
	angle(angle_)
{
	b = CalcB(a, length, angle_);
}

glm::vec2 ocbj::bone_2d::CalcB(const glm::vec2& origin, float length, float angle) {
	return origin + polarToCartesian(kDeg2Rad(angle), length);
}

void ocbj::fabik2d::Update(std::vector<bone_2d>& bones, glm::vec2& start_effector, 
	glm::vec2& end_effector) {
	if (bones.empty())
		return;

	bones.back().a = start_effector;

	// move towards the end effector (the forwards part!)
	glm::vec2* target_position = &end_effector;

	for (bone_2d& bone : bones) {
		bone.b = bone_2d::CalcB(bone.a, bone.length, bone.angle);
		Follow(bone, *target_position);
		target_position = &bone.a;
	}

	// move towards the start effector (the backwards part!)
	target_position = &start_effector;

	for (bone_2d& bone : bones | std::views::reverse) {
		Follow(bone, *target_position);
		target_position = &bone.a;
	}
}

void ocbj::fabik2d::Follow(bone_2d& bone, const glm::vec2& target) {
	glm::vec2 direction = glm::normalize(target - bone.a);

	float next_angle = kRad2Deg(atan2(direction.y, direction.x));

	if (isnan(next_angle))
		return;

	bone.angle = next_angle;
	bone.a = target + (-direction * bone.length);
}
