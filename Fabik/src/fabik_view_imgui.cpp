#include <imgui/imgui.h>

#include "target2d.h"
#include "fabik_view.h"

// An implementation of our fabik view using ImGui

constexpr float kAlternateValue(int i, float a, float b) { return i % 2 == 0 ? a : b; }

void ocbj::fabik_view::Init(ocbj::target_2d& target) {
	_target = &target;

	float inital_angle = 90.0F;

	// first bone is our fixed origin (the root of the chain) 
	glm::vec2 root(0.0F, _inital_bone_length);
	_bones.emplace_back(root, _inital_bone_length, inital_angle);

	AddBones(_inital_number_of_bones - 1);
}

void ocbj::fabik_view::Draw() const {
	ImGui::SetNextWindowPos(ImVec2(0.0F, 0.0F));
	ImGui::SetNextWindowSize(ImVec2(_size.x, _size.y));

	static bool window_open = true;
	static int window_flags = ImGuiWindowFlags_NoBackground | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoInputs;
	
	ImGui::Begin("fabik_view", &window_open, window_flags);
		if (ImDrawList* draw_list = ImGui::GetWindowDrawList(); draw_list != nullptr) {

			if (_target != nullptr) {
				ImU32 color = IM_COL32((int)_target_color.r, (int)_target_color.g, (int)_target_color.b, 255);
				draw_list->AddCircleFilled(ImVec2(_target->position.x, _target->position.y), 15.0F, color, 255);
			}

			// draw the chain!
			float i = 0.0F;
			for (const bone_2d& bone : _bones) {
				draw_list->AddLine(
					ImVec2(bone.a.x, bone.a.y),
					ImVec2(bone.b.x, bone.b.y),
					IM_COL32((i / _bones.size() * 255), kAlternateValue(i, 25, 150), 255, 255),
					8
				);

				// visualize where the joint would be as a circle.
				draw_list->AddCircleFilled(ImVec2(bone.a.x, bone.a.y), 8.0F, IM_COL32(222, 255, 180, 64));
				++i;
			}
		}
	ImGui::End();
}

void ocbj::fabik_view::Update(double deltaTime, double elapsedTime, const glm::vec2& size) {
	_size = size;

	glm::vec2 start_effector = glm::vec2(size) / 2.0F;
	start_effector.y = size.y;

	if (_target != nullptr) {
		_target_color.r = 128.0F * abs(sin(elapsedTime));
		_target_color.b = 255.0F * abs(cos(elapsedTime));
		fabik2d::Update(_bones, start_effector, _target->position);
	}
}

void ocbj::fabik_view::AddBones(int count) {
	if (count < 1)
		return;

	for (size_t i = 0; i < count; i++) {
		bone_2d& parent = _bones.back();
		_bones.emplace_back(parent, parent.length, parent.angle);
	}
}

void ocbj::fabik_view::RemoveBones(int count) {
	if (_bones.size() - count < 1)
		return; // need at least one bone the chain

	_bones.pop_back();
}

void ocbj::fabik_view::SetBoneLength(float length) {
	for (bone_2d& bone : _bones) 
		bone.length = length;
}

float ocbj::fabik_view::BoneLength() {
	if (_bones.empty())
		return _inital_bone_length;

	return _bones.back().length;
}

int ocbj::fabik_view::NumberOfBones() {
	return _bones.size();
}
