#include <vector>

#include <GL/gl3w.h>            
#include <glfw/glfw3.h>

#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

#include "iview.h"
#include "ui.h"

// An implementation of our simple ui using ImGui

ocbj::ui::ui(const char* glsl_version, GLFWwindow* window) {
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    const ImGuiIO& io = ImGui::GetIO(); (void)io;

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);
};

ocbj::ui::~ui() {
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
}

void ocbj::ui::Begin() const {
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}

void ocbj::ui::Draw(std::vector<iview*>& views) const {
    for (iview* view : views)
        if(view != nullptr)
            view->Draw();
}

void ocbj::ui::Update(double deltaTime, double elapsedTime, const glm::vec2& size, std::vector<iview*>& views) {
    for (iview* view : views)
        if(view != nullptr)
            view->Update(deltaTime, elapsedTime, size);
}

void ocbj::ui::End() const {
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}
