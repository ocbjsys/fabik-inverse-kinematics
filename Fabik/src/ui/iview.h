#pragma once
#include <glm/glm.hpp>

namespace ocbj {
	class iview {
	public:
		virtual void Draw() const = 0;
		virtual void Update(double deltaTime, double elapsedTime, const glm::vec2& size) = 0;
	};
}
