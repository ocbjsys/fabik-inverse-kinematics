#pragma once
#include <glm/glm.hpp>

#include "iview.h"

struct GLFWwindow;

namespace ocbj {
    class ui {
    public:
        ui(const char* glsl_version, GLFWwindow* window);
        virtual ~ui();

        void Begin() const;
        void Draw(std::vector<iview*>& views) const;
        void Update(double deltaTime, double elapsedTime, const glm::vec2& size, std::vector<iview*>& views);
        void End() const;
    };
}
