#include <imgui/imgui.h>

#include "fabik_options_view.h"

void ocbj::fabik_options_view::Init(fabik_view* fabik_view_) {
	_fabik_view = fabik_view_;
}

void ocbj::fabik_options_view::Draw() const {
	if (_fabik_view == nullptr)
		return;

	ImGui::SetNextWindowPos(ImVec2(8.0F, 8.0F)); 
	ImGui::SetNextWindowSize(ImVec2(0.0F, 0.0F)); // fit to content

	static bool window_open = true;

	// Display window as an overlay
	ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | 
		ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | 
		ImGuiWindowFlags_NoNav | ImGuiWindowFlags_NoMove;

	ImGui::Begin("Options", &window_open, window_flags);

		// Number of bones
		static int max_number_of_bones = 24;
		static int min_number_of_bones = 1;

		int number_of_bones = _fabik_view->NumberOfBones();
		int prev_number_of_bones = number_of_bones;

		ImGui::SliderInt("Count", &number_of_bones, min_number_of_bones, max_number_of_bones);

		if (number_of_bones > prev_number_of_bones)
			_fabik_view->AddBones(1);
		else if (number_of_bones < prev_number_of_bones)
			_fabik_view->RemoveBones(1);

		// Bone length
		static float min_bone_length = 6.0F;
		static float max_bone_length = 24.0F;

		float bone_length = _fabik_view->BoneLength();
		float prev_bone_length = min_bone_length;

		ImGui::SliderFloat("Length", &bone_length, min_bone_length, max_bone_length);

		if (abs(prev_bone_length - bone_length) > 0.001F) 
			_fabik_view->SetBoneLength(bone_length);

	ImGui::End();
}

void ocbj::fabik_options_view::Update(double deltaTime, double elapsedTime, const glm::vec2& size) /*override*/ {
	// Nothing todo
}
