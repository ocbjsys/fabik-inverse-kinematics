#pragma once
#include <vector>

#include <glm/glm.hpp>

namespace ocbj {
	// Represents a segment in a kinematic chain
	struct bone_2d {
		// Point representing Cartesian coordinates in world space (our origin/start)
		glm::vec2 a;

		// Point representing Cartesian coordinates in world space (our end)
		glm::vec2 b; 

		// Length from point a to point b
		float length;

		// Angle in world space (in degrees)
		// (For more practial applications, we would want an an angle that is relative to the parent)
		float angle; 

		// Creates an instance of bone, setting the origin point a to be it's parents end point b 
		bone_2d(const bone_2d& parent, float length_, float angle_);

		// Creates a bone 
		bone_2d(const glm::vec2& a_, float length_, float angle_);

		// Calculates the Cartesian coordinates of point b, given the 
		// Polar coordinates (point a, angle and length)
		static glm::vec2 CalcB(const glm::vec2& point, float length, float angle);
	};

	// Forwars and backwards inverse kinematics
	class fabik2d {
	public:
		static void Update(std::vector<bone_2d>& bones, 
			glm::vec2& start_effector,	// Target for the start of the kinematic chain
			glm::vec2& end_effector		// Target for the end of the kinematic chain
		);

	private:
		static void Follow(bone_2d& bone, const glm::vec2& target);
	};
}
