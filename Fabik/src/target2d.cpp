#include <random>

#include "target2d.h"

ocbj::target_2d::target_2d(float speed_ /*= 1.0F*/,
	const glm::vec2& position_ /*= glm::vec2(0.0F, 0.0F)*/,
	const glm::vec2& direction_ /*= glm::vec2(1.0F, 1.0F)*/)
	: speed(speed_),
	position(position_),
	direction(direction_)
{ }

void ocbj::target_2d::Update(double deltaTime, double elapsedTime, const glm::vec2& bounds) {
	position += speed * (float)deltaTime * direction;

	static std::default_random_engine generator;
	static std::uniform_real_distribution<float> distribution(0.5F, 1.0F);

	// invert the direction when we reach a planes bounds
	// using a random amount makes for a less boring simulation 
	float amount = distribution(generator);

	if (position.x >= bounds.x) 
		direction.x = -amount;
	
	if (position.x <= 0.0F) 
		direction.x = amount;
	
	if (position.y >= bounds.y) 
		direction.y = -amount;

	if (position.y <= 0.0F) 
		direction.y = amount;
}
